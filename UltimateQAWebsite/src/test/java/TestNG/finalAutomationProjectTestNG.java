package TestNG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.AutomateApplication;
import Pages.BigPageWithManyElements;
import Pages.FakeLandingPage;
import Pages.FakePricingPage;
import Pages.FillOutForms;
import Pages.FillingOutForms;
import Pages.InteractionsWithSimpleElements;
import Pages.LoginAutomationPage;

public class finalAutomationProjectTestNG{

static WebDriver driver;

@BeforeSuite

public void executeBeforeStartingTest() {
	
}
@BeforeTest
public static void invokeBrowser() {
	
System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
driver = new ChromeDriver();
driver.navigate().to("https://ultimateqa.com/automation/");
driver.manage().window().maximize();
// Wait for the browser to load
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

}
@Test(priority =0)

private static void BigPageWithManyElements() throws InterruptedException {
	
BigPageWithManyElements obj3 = new BigPageWithManyElements(driver);

obj3.clickBigPageWithManyElements();
obj3.clickButton();
obj3.clickButton1();
obj3.clickButton2();
obj3.clickButton3();
obj3.clickButton4();
obj3.clickButton5();
obj3.clickButton6();
obj3.clickButton7();
obj3.clickButton8();
obj3.clickButton9();
obj3.enterName("Anu");
obj3.enterEmail("anuneravetla3010@gmail.com");
obj3.enterMessage("Test Message");

}

@Test(priority =1)
private static void FakeLandingPage() {
	
	FakeLandingPage obj4 = new FakeLandingPage(driver);
	  obj4.clickFakeLandingPage(); obj4.clickViewCourse();
	
	
}
@Test(priority =2)
private static void FakePricingPage() throws InterruptedException {
	
	FakePricingPage obj5 = new FakePricingPage(driver);
	  obj5.clickFakePricingPage(); obj5.clickPurchase();
	  
	
}
@Test(priority =3)

private static void FillOutForms() {
	
	 FillOutForms obj = new FillOutForms(driver);
	  
	  obj.clickFillOutForms();
	
}
@Test(priority=4)
private static void FillingOutForms() throws InterruptedException {
	
	 FillingOutForms obj1 = new FillingOutForms(driver); 
	 obj1.enterName("Anu");
	  obj1.enterMessage("Test Message"); obj1.clickSubmit();
	  obj1.enterName1("Teja"); obj1.enterMessage1("Test Message");
	  String capchavalue = driver.findElement(By.
			  xpath("//span[@class ='et_pb_contact_captcha_question']")).getText() .trim();
			  String removespace = capchavalue.replaceAll("\\s+", ""); String[] parts =
			  removespace.split("\\+"); String part1 = parts[0]; String part2 = parts[1];
			  String[] parts1 = part2.split("\\="); String Lastpart = parts1[0]; int
			  summation = Integer.parseInt(part1) + Integer.parseInt(Lastpart); String s =
			  String.valueOf(summation); 
			  obj1.enterNumber(s); 
			  obj1.clickSubmit1();
	}
@Test(priority =5)
private static void AutomateApplication() {
	
	 AutomateApplication obj6 = new AutomateApplication(driver);
	  
	  obj6.clickLearnHowToAutomateAnApplication(); 
	  obj6.enterFirstName("Anu");
	  obj6.clickSubmit();
	
	
}
@Test(priority =6)
private static void LoginAutomationPage() {
	LoginAutomationPage obj2 = new LoginAutomationPage(driver);
	  obj2.clickLoginAutomation(); 
	  obj2.enterEmail("anuneravetla3010@gmail.com");
	  obj2.enterPassword("Hanumanthu@05"); 
	  obj2.clickSignIn();
	  
	
}
@Test(priority =7)
private static void InteractionWithSimpleElements() throws InterruptedException {
	
	InteractionsWithSimpleElements obj7 = new
			  InteractionsWithSimpleElements(driver);
			  
			  obj7.clickInteractionsWithSimpleElements(); 
			  obj7.clickClickMe();
			  
			 
}
@AfterTest 
private static void CloseBrowser() {
	// TODO Auto-generated method stub

}
}




