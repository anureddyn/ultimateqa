package Test;

//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

//import Configuration.propertiesfile;
import Pages.BigPageWithManyElements;
import Pages.FakeLandingPage;
import Pages.AutomateApplication;


import Pages.FakePricingPage;

import Pages.FillOutForms;
import Pages.FillingOutForms;

import Pages.InteractionsWithSimpleElements;


public class AutomationPageTest {

	private static WebDriver driver ;
	public static String browserName = null;

	public static void main(String[] args) throws InterruptedException {

		invokeBrowser();

		SignUpflow();

		CloseBrowser();

	}

	private static void invokeBrowser() {
		// TODO Auto-generated method stub

			System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
			driver = new ChromeDriver();
		}
	

	private static void SignUpflow() throws InterruptedException {
		// TODO Auto-generated method stub

		BigPageWithManyElements obj3 = new BigPageWithManyElements(driver);

		obj3.clickBigPageWithManyElements();
		obj3.clickButton();
		obj3.clickButton1();
		obj3.clickButton2();
		obj3.clickButton3();
		obj3.clickButton4();
		obj3.clickButton5();
		obj3.clickButton6();
		obj3.clickButton7();
		obj3.clickButton8();
		obj3.clickButton9();
		obj3.enterName("Anu");
		obj3.enterEmail("anuneravetla3010@gmail.com");
		obj3.enterMessage("Test Message");

		
		  FakeLandingPage obj4 = new FakeLandingPage(driver);
		  obj4.clickFakeLandingPage(); obj4.clickViewCourse();
		  
		 FakePricingPage obj5 = new FakePricingPage(driver);
		  obj5.clickFakePricingPage(); obj5.clickPurchase();
		  
		  FillOutForms obj = new FillOutForms(driver);
		  
		  obj.clickFillOutForms();
		  
		  FillingOutForms obj1 = new FillingOutForms(driver); obj1.enterName("Anu");
		  obj1.enterMessage("Test Message"); obj1.clickSubmit();
		  obj1.enterName1("Teja"); obj1.enterMessage1("Test Message");
		  
		  String capchavalue = driver.findElement(By.
		  xpath("//span[@class ='et_pb_contact_captcha_question']")).getText() .trim();
		  String removespace = capchavalue.replaceAll("\\s+", ""); String[] parts =
		  removespace.split("\\+"); String part1 = parts[0]; String part2 = parts[1];
		  String[] parts1 = part2.split("\\="); String Lastpart = parts1[0]; int
		  summation = Integer.parseInt(part1) + Integer.parseInt(Lastpart); String s =
		  String.valueOf(summation); 
		  obj1.enterNumber(s); 
		  obj1.clickSubmit1();
		  
		  AutomateApplication obj6 = new AutomateApplication(driver);
		  
		  obj6.clickLearnHowToAutomateAnApplication(); obj6.enterFirstName("Anu");
		  obj6.clickSubmit();
		  
		  
		  InteractionsWithSimpleElements obj7 = new
		  InteractionsWithSimpleElements(driver);
		  
		  obj7.clickInteractionsWithSimpleElements(); 
		  obj7.clickClickMe();
		  
	}

	private static void CloseBrowser() {
		// TODO Auto-generated method stub

	}
}
