package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FillOutForms {

	    WebDriver driver = null;
	    
	 static WebElement element = null;
	    
	    By clickFillOutForms = By.xpath("//div[@class='et_pb_text_inner']/ul/li[4]/a");
	    
	    public FillOutForms(WebDriver driver) {
			this.driver = driver;
		}
	    
	    public void clickFillOutForms()
	    {
	    	driver.findElement(clickFillOutForms).click();
	    	
	    }
	    
}