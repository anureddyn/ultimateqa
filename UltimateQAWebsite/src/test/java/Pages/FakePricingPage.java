package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FakePricingPage {

	
	WebDriver driver = null;
	WebElement element = null;
	
	By clickFakePricingPage = By.xpath("//div[@class='et_pb_text_inner']/ul/li[3]/a");
	By clickPurchase = By.xpath("(//a[@class='et_pb_button et_pb_pricing_table_button'])[1]");
	 
	public FakePricingPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickFakePricingPage() throws InterruptedException {
		driver.findElement(clickFakePricingPage).click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)","");
		TimeUnit.SECONDS.sleep(5);
		js.executeScript("window.scrollBy(0,-1000)","" );

	}
	public void clickPurchase() throws InterruptedException {
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(clickPurchase).click();
		driver.navigate().to("https://ultimateqa.com/automation/");
	}
}
