package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AutomateApplication {

	WebDriver driver = null;
	WebElement element = null;
	
	By clickLearnHowToAutomateAnApplication = By.xpath("//div[@class='et_pb_text_inner']/ul/li[5]/a");
	By enterFirstName = By.xpath("//input[@name = 'firstname']");
	By clickSubmit = By.xpath("//input[@id = 'submitForm']");
	
	public AutomateApplication(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickLearnHowToAutomateAnApplication() {
		driver.findElement(clickLearnHowToAutomateAnApplication).click();
	}
	public void enterFirstName(String FirstName) {
		driver.findElement(enterFirstName).sendKeys(FirstName);
	}
	public void clickSubmit() {
		driver.findElement(clickSubmit).click();
		driver.navigate().to("https://ultimateqa.com/automation/");
	}
}
