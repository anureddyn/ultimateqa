package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BigPageWithManyElements {

	WebDriver driver = null;
	WebElement element = null;

	By clickBigPageWithManyElements = By.xpath("//div[@class='et_pb_text_inner']/ul/li[1]/a");
	By clickButton = By.xpath("//a[@class ='et_pb_button et_pb_button_0 et_pb_bg_layout_light']");
	By clickButton1 = By.xpath("//a[@class ='et_pb_button et_pb_button_1 et_pb_bg_layout_light']");
	By clickButton2 = By.xpath("//a[@class ='et_pb_button et_pb_button_2 et_pb_bg_layout_light']");
	By clickButton3 = By.xpath("//a[@class ='et_pb_button et_pb_button_6 et_pb_bg_layout_light']");
	By clickButton4 = By.xpath("//a[@class ='et_pb_button et_pb_button_7 et_pb_bg_layout_light']");
	By clickButton5 = By.xpath("//a[@class ='et_pb_button et_pb_button_3 et_pb_bg_layout_light']");
	By clickButton6 = By.xpath("//a[@class ='et_pb_button et_pb_button_4 et_pb_bg_layout_light']");
	By clickButton7 = By.xpath("//a[@class ='et_pb_button et_pb_button_5 et_pb_bg_layout_light']");
	By clickButton8 = By.xpath("//a[@class ='et_pb_button et_pb_button_9 et_pb_bg_layout_light']");
	By clickButton9 = By.xpath("//a[@class ='et_pb_button et_pb_button_10 et_pb_bg_layout_light']");
	By clickTwitter = By.xpath(
			"//ul[@class ='et_pb_module et_pb_social_media_follow et_pb_social_media_follow_0 clearfix et_pb_bg_layout_light']");
	By enterName = By.xpath("//input[@name = 'et_pb_contact_name_0']");
	By enterEmail = By.xpath("//input[@name = 'et_pb_contact_email_0']");
	By enterMessage = By.xpath("//textarea[@id='et_pb_contact_message_0']");

	public BigPageWithManyElements(WebDriver driver) {
		this.driver = driver;
	}

	public void clickBigPageWithManyElements() {

		driver.findElement(clickBigPageWithManyElements).click();
	}

	public void clickButton()  {

		driver.findElement(clickButton).click();

	}

	public void clickButton1()  {

		driver.findElement(clickButton1).click();

	}

	public void clickButton2() throws InterruptedException {

		driver.findElement(clickButton2).click();
		TimeUnit.SECONDS.sleep(5);
	}

	public void clickButton3()  {

		driver.findElement(clickButton3).click();

	}

	public void clickButton4()  {

		driver.findElement(clickButton4).click();

	}

	public void clickButton5() throws InterruptedException {

		driver.findElement(clickButton5).click();
		TimeUnit.SECONDS.sleep(5);
	}

	public void clickButton6() {

		driver.findElement(clickButton6).click();

	}

	public void clickButton7() {

		driver.findElement(clickButton7).click();

	}

	public void clickButton8()  {

		driver.findElement(clickButton8).click();
	
	}

	public void clickButton9() throws InterruptedException {

		driver.findElement(clickButton9).click();
		TimeUnit.SECONDS.sleep(10);
	}

	public void enterName(String Name) {
		driver.findElement(enterName).sendKeys(Name);
	}

	public void enterEmail(String Email) throws InterruptedException {
		driver.findElement(enterEmail).sendKeys(Email);

	}

	public void enterMessage(String Message) throws InterruptedException {
		driver.findElement(enterMessage).sendKeys(Message);
		TimeUnit.SECONDS.sleep(10);
		driver.navigate().to("https://ultimateqa.com/automation/");
	}
     
}
