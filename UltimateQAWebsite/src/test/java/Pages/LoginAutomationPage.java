package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginAutomationPage {
	
	WebDriver driver = null;
	WebElement element = null;
	
	By clickLoginAutomation  = By.xpath("//div[@class='et_pb_text_inner']/ul/li[6]/a");
    By enterEmail = By.xpath("//input[@id='user[email]']");
    By enterPassword = By.xpath("//input[@id='user[password]']");
    By clickRememberMe = By.xpath("//input[@id='user[remember_me]']");
    By clickSignIn = By.xpath("//input[@value='Sign in']");
    
    
    public LoginAutomationPage(WebDriver driver) {
		this.driver = driver;
    }
		
		 public void clickLoginAutomation(){
		    	driver.findElement(clickLoginAutomation).click();
		    	
		    }
		 public void enterEmail(String Email) {
		  		driver.findElement(enterEmail).sendKeys(Email); 
		  	}
		 public void enterPassword(String Password) {
		  		driver.findElement(enterPassword).sendKeys(Password); 
		  	}
		 public void clickRememberMe(){
		    	driver.findElement(clickRememberMe).click();
		    }
		 
		 public void clickSignIn(){
		    	driver.findElement(clickSignIn).click();
		    	driver.navigate().to("https://ultimateqa.com/automation/");
		    }
	}

