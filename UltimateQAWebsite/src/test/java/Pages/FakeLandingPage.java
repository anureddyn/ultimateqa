package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FakeLandingPage {

	WebDriver driver = null;
	WebElement element = null;
	
	By clickFakeLandingPage = By.xpath("//div[@class='et_pb_text_inner']/ul/li[2]/a");
	By clickViewCourse = By.xpath("(//a[@class='et_pb_button et_pb_custom_button_icon et_pb_button_0 et_hover_enabled et_pb_bg_layout_light'])");

    public FakeLandingPage(WebDriver driver) {
    	this.driver = driver;
    }

    public void clickFakeLandingPage(){
    	driver.findElement(clickFakeLandingPage).click();
    	
    }
    public void clickViewCourse(){
    	driver.findElement(clickViewCourse).click();
    	driver.navigate().to("https://ultimateqa.com/automation/");
    	
    }

}
