package Pages;

import java.util.concurrent.TimeUnit;

//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FillingOutForms {
	
	WebDriver driver = null;
	WebElement element = null;

	By enterName = By.xpath("//input[@id='et_pb_contact_name_0']");
	By enterMessage = By.xpath("//textarea[@placeholder='Message']");
	By clickSubmit = By.xpath("//button[@name='et_builder_submit_button']");
	By enterName1 = By.xpath("//input[@id='et_pb_contact_name_1']");
	By enterMessage1 = By.xpath("//textarea[@id='et_pb_contact_message_1']");
	By enterNumber = By.xpath("//input[@class='input et_pb_contact_captcha']");
	By clickSubmit1 = By.xpath("//*[@id=\"et_pb_contact_form_1\"]/div[2]/form/div/button");
	
	public FillingOutForms(WebDriver driver) {
		this.driver = driver;
		
	}
	public void enterName(String Name) {
		
  		driver.findElement(enterName).sendKeys(Name); 
  	}
    
	public void enterMessage(String Message) {
		driver.findElement(enterMessage).sendKeys(Message);
	}
	
	 public void clickSubmit()  {
	    	driver.findElement(clickSubmit).sendKeys(Keys.ENTER);
	    }
	 
	 public void enterName1(String Name) {
	  		driver.findElement(enterName1).sendKeys(Name); 
	  	}
	 public void enterMessage1(String Message) {
			driver.findElement(enterMessage1).sendKeys(Message);
		}
	 public void enterNumber(String Number) {
			driver.findElement(enterNumber).sendKeys(Number);
			
		}
	 public void clickSubmit1() throws InterruptedException {
		 TimeUnit.SECONDS.sleep(5);
	    	driver.findElement(clickSubmit1).sendKeys(Keys.ENTER);
	    	TimeUnit.SECONDS.sleep(5);
	    	driver.navigate().to("https://ultimateqa.com/automation/");
	 }
	 
}	 
	 
	 /*public void clickSubmit1() throws InterruptedException{
	    	driver.findElement(clickSubmit1).sendKeys(Keys.ENTER);
	    	By capchaQuestion = null;
			String capchavalue = driver.findElement(capchaQuestion).getText();
	    	int firstinteger= Integer.parseInt(capchavalue.substring(0, 2));
	    	int secondinteger= Integer.parseInt(capchavalue.substring(5, 6));
	    	int calc= firstinteger+secondinteger;
	    	String final_value= String.valueOf(calc);
	    	//driver.findElement(By.xpath("")).sendKeys(final_value);



			//driver.findElement(fillingOutFo).clear();
	    	driver.findElement(clickSubmit).sendKeys(final_value);
	    	TimeUnit.SECONDS.sleep(3); */

	 
	    	
	 
             

