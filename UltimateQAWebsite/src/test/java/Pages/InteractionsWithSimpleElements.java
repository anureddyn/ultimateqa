package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InteractionsWithSimpleElements {
	
	WebDriver driver = null;
	WebElement element = null;
	
	By clickInteractionsWithSimpleElements = By.xpath("//div[@class='et_pb_text_inner']/ul/li[7]/a");
	By clickClickMe = By.xpath("//button[@id='button1']");
	By clickRaise = By.xpath("//button[@id='button2']");
	By clickThisButtonUsingID = By.xpath("//a[@id='idExample']");
	
	public InteractionsWithSimpleElements(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickInteractionsWithSimpleElements() {
		driver.findElement(clickInteractionsWithSimpleElements).click();
	}
	public void clickClickMe() {
		driver.findElement(clickClickMe).click();
	}
	
	public void clickRaise() {
		driver.findElement(clickRaise).click();
	}
	public void clickThisButtonUsingId() {
		driver.findElement(clickThisButtonUsingID).click();
		driver.navigate().to("https://ultimateqa.com/automation/");
	}

}
